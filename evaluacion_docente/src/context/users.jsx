export function loadUser() {
	
	let user = window.localStorage.getItem("proyectousr");
	if ( user ) {
		user = JSON.parse(user);
		this.setState({user: user});
		return user;
	}
}
export function typeUser(typeUser) {		
	window.localStorage.setItem("tipo_eval", JSON.stringify(typeUser));
	//	this.setState({typeUser});		
}

export function saveRegister(data) {
	this.setState({registerData: data});
}

export function logIn(data) {
	window.localStorage.setItem("proyectousr", JSON.stringify(data));
	this.setState({user: data});
}

export function logOut() {
	window.localStorage.setItem("proyectousr", '{"auth": false}');
	window.localStorage.removeItem("proyectousr");
	this.setState({ user: { auth: false } });
}