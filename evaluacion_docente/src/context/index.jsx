/**
 *  index.js
 *  @version: 1.0.0
 *  @author: Fernando Aban
 *  @description: Manejador principal del estado global de la aplicación
 */

import React, { Component, createContext } from "react";


// Instancia del Context, métodos: Provider y Consumer
import * as action from "./users"
const Context = createContext();

class GlobalContext extends Component {

	constructor(props) {
    	super(props);
    	this.state = {
		typeUser: action.typeUser.bind(this),
		loadUser: action.loadUser.bind(this)
    	};
  	}

	componentDidMount() {
		this.state.loadUser();
		
	}

	render() {
    	return (
      		<Context.Provider value={this.state}>
        		{ this.props.children }
      		</Context.Provider>
    	);
  	}
}


/**
 * @function: Consumer
 * @description: HOC conector entre el estado global y un componente consumidor.
 * @param: Component => Componente Web
 *
 */

const Consumer = Component => {
	return props => {
    	return (
      		<Context.Consumer>
        		{ context => <Component {...props} context={context} /> }
      		</Context.Consumer>
    	);
  	};
};

export { Consumer, GlobalContext };