/**
 *  app.js
 *  @version: 1.0.0
 *  @author: Fernando Aban
 *  @description: Punto de Entrada de la aplicación
 * 	@process: 2
*/

import React,{Component} from 'react';
import Views from './core/views';
import WOW from "wowjs";

//process.env.AUTH = '908u90u09u099u09u0';


class App extends Component{
	componentDidMount() {
		new WOW.WOW().init();
	}

	render(){
		return(
			<div className="App">
		<Views></Views>
	</div>
		)
	}
}

export default App;