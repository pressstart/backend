/**
 *  routes.jsx
 *  @version: 1.0.0
 *  @author: Fernando Aban
 *  @description: Manejo de todas las rutas de la aplicación
 * 	@process: 4
 */

import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

// Conexión con el Context API
import { GlobalContext } from "../context";

// Páginas Web
import Home from "../pages/home";
import Login from "../pages/login";
import Preguntas from "../pages/preguntas";
import Reporte from "../pages/reporte";

class Routes extends Component {
  render() {
    return (
      <GlobalContext>
        <BrowserRouter>
          <div className="flex main">             
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/home" component={Home} />
                <Route path="/login" component={Login} />
                <Route path="/reporte" component={Reporte} />
                <Route path="/preguntas" component={Preguntas} />
              </Switch>           
          </div>
        </BrowserRouter>
      </GlobalContext>
    );
  }
}

export default Routes;
