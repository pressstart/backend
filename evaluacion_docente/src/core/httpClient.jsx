/**
 *  httpClient.js
 *  @version: 1.0.0
 *  @author: Fernando Aban
 *  @description: Cliente HTTP para todas las peticiones Web
 */

import request from "superagent";
import { URL_API } from './urlsApi';

class Request {
  get(url, data) {
    const result = request
      .get( URL_API + url )
      .query(data) 
      .set({'auth': '8f841fe16f2920045fa988929e1692df6d8a5b6837c5df22c448e51a9ddbfc3b'})
      .then(res => {
        return res.body;
      })
      .catch(err => {
        console.log(err.message);
        return { error: true, message: err.message };
      });
    return result;
  }

  post(url, data) {
    console.log(url,data)
    const result = request
      .post( URL_API + url )
      .set({'auth': '8f841fe16f2920045fa988929e1692df6d8a5b6837c5df22c448e51a9ddbfc3b'})
      .send(data)
      .then(res => {
        return res.body;
      })
      .catch(err => {
        console.log(err.message);
        return { error: true, message: err.message };
      });
    return result;
  }
}

export default Request;