/**
 *  urlApi.js
 *  @version: 1.0.0
 *  @author: Fernando Aban
 *  @description: Manejor de las ruta directas al API
 */

function url() {
    let url = "http://localhost:8081";
    if (process.env.NODE_ENV === "production") {
      url = "ip del servidor";
    }
    return url;
  }
  
  export const URL_API = url();