import React, {Component} from 'react';
import Header from "../components/header";

class Preguntas extends Component{
    render(){
        return(
            <div className="survey column">
                 < Header />
                <div className="content row-responsive">
                <div className="left column">
                    <div className="title">
                        <h4>
                            Lista de docentes
                        </h4>
                    </div>
                    <div className="content column">
                        <div className="evaluated align-center">
                            <div className="name">
                                <h4 className="font-small">
                                    MIT. José Mario Guitierrez Perez
                                </h4>
                            </div>
                            <div className="option first-option align-center justify-center color-dimGray">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </div>
                        <div className="evaluated align-center">
                            <div className="name">
                                <h4 className="font-small">
                                    Lic. Carlos Martín Tamay
                                </h4>
                            </div>
                            <div className="option align-center justify-center color-dimGray">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </div>
                        <div className="evaluated evaluated-complete align-center">
                            <div className="name">
                                <h4 className="font-small">
                                    Dr. Mariano Xui Chan
                                </h4>
                            </div>
                            <div className="option align-center justify-center color-dimGray">
                                <i className="fas fa-check"></i>
                            </div>
                        </div>
                        <div className="evaluated align-center">
                            <div className="name">
                                <h4 className="font-small">
                                    Lic. Felipe Calderón Hinojosa
                                </h4>
                            </div>
                            <div className="option align-center justify-center color-dimGray">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </div>
                        <div className="evaluated evaluated-active align-center">
                            <div className="name">
                                <h4 className="font-small">
                                    MTI. Jennifer Vanessa Dominguez Rojas
                                </h4>
                            </div>
                            <div className="option align-center justify-center color-dimGray">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </div>
                        <div className="evaluated align-center">
                            <div className="name">
                                <h4 className="font-small">
                                    Dr. Sergio Alejandro Trejo Cuxim
                                </h4>
                            </div>
                            <div className="option align-center justify-center color-dimGray">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </div>
                        <div className="evaluated align-center">
                            <div className="name">
                                <h4 className="font-small">
                                    CP. Katherine Felix Bernaal
                                </h4>
                            </div>
                            <div className="option align-center justify-center color-dimGray">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </div>
                        <div className="evaluated align-center">
                            <div className="name">
                                <h4 className="font-small">
                                    Ing. Reynaldo Guzmán Valdivieso
                                </h4>
                            </div>
                            <div className="option align-center justify-center color-dimGray">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </div>
                    </div>
                    <div className="exit">
                        <a href="#" className="weight-semi">
                            <i className="fas fa-times"></i>&nbsp; Cerrar sesión
                        </a>
                    </div>
                </div>
                <div className="right">
                    <div className="sheet column">
                        <div className="title column">
                            <h3>
                                Evaluando a: MTI. Jennifer Vanessa Dominguez Rojas
                            </h3>
                            <div className="white-space-8"></div>
                            <p>
                                Selecciona la opción correspondiente de la más <b>mala</b> a la <b>excelente</b>
                            </p>
                        </div>
                        <div className="white-space-32"></div>
                        <div className="questions column" id="question1">
                            <div className="question">
                                <p className="text-justify">
                                    1.- ¿Las instalaciones de la universidad favorecen al desarrollo completo de la carrera de inhtmlFormática?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer1" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer1" value="2" id="radio-2" className="radio" defaultChecked/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer1" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer1" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer1" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis, itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis,
                                    itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis,
                                    itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis,
                                    itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis, itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis, itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis, itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis, itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis,
                                    itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>
                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis,
                                    itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    2.- ¿Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum omnis ut non architecto debitis,
                                    itaque error tenetur laudantium odit ab similique est incidunt fuga veniam ratione ipsam animi corrupti?
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <div className="answers">
                                <input type="radio" name="answer2" value="1" id="radio-1" className="radio"/>
                                <label htmlFor="radio-1">Mala</label>
                                <input type="radio" name="answer2" value="2" id="radio-2" className="radio"/>
                                <label htmlFor="radio-2">Deficiente</label>
                                <input type="radio" name="answer2" value="3" id="radio-3" className="radio"/>
                                <label htmlFor="radio-3">Regular</label>
                                <input type="radio" name="answer2" value="4" id="radio-4" className="radio"/>
                                <label htmlFor="radio-4">Buena</label>
                                <input type="radio" name="answer2" value="5" id="radio-5" className="radio"/>
                                <label htmlFor="radio-5">Excelente</label>
                            </div>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="questions column" id="question2">
                            <div className="question">
                                <p className="text-justify">
                                    Comentario adicional:
                                </p>
                            </div>
                            <div className="white-space-16"></div>
                            <textarea name="comments" maxLength="500" cols="30" rows="10" placeholder="Comentario adicional, opcional"></textarea>
                        </div>

                        <div className="white-space-32"></div>
                        <div className="btn-container">
                            <button type="submit" className="btn btn-darkBlue weight-semi color-white">
                                <i className="fas fa-check"></i>&nbsp; FINALIZAR EVALUACIÓN
                            </button>
                        </div>
                        <div className="white-space-24"></div>

                    </div>
                </div>
            </div>

            </div>
        )
    }
}
export default Preguntas;