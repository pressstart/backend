/**
 *  Home.js
 *  @version: 1.0.0
 *  @author: Fernando Aban
 *  @description: Punto de Entrada de la aplicación
 */
import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { withRouter } from "react-router-dom";
import { Consumer } from "../context";
import Fondo from "../img/leftblue.jpg"
import Logo from "../img/upqroo.png"

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  mensaje() {

  }
  typeUser(tipo) {
    this.props.context.typeUser({ typeUser: tipo });

  }

  render() {
    return (
      <div className="column login">
        <div className="row background-login" >
          <div className="column justify-center align-center">
            <div className="row">
              <div className="column left justify-center align-center" style={{ backgroundImage: "url(" + Fondo + ")" }}>
                <div className="container responsive-img item-left justify-center align-center">
                  <img src={Logo} alt="Universidad Politecnica" title="Universidad Politecnica" className="cover-img logo" />
                </div>
              </div>

              <div className="column right align-center justify-center">
                <div className="row container justify-center">
                  <div className="column form-data" >

                    <h1 className="color-darkBlue font-double text-center title">EVALUACIÓN DOCENTE</h1>
                    <div className="white-space-32"></div>
                    <Link className="btn btn-login  bg-darkBlue color-white font-regular weight-semi" to="/login" onClick={this.typeUser.bind(this, "alumno")}>Alumno - Docente </Link>
                    <div className="white-space-32"></div>
                    <Link className="btn btn-login  bg-darkBlue color-white font-regular weight-semi" to="/login" onClick={this.typeUser.bind(this, "docente")}>Docente - Docente </Link>
                    <div className="white-space-32"></div>
                    <Link type="submit" className="btn btn-login  bg-darkBlue color-white font-regular weight-semi" to="/login" onClick={this.typeUser.bind(this, "docente")}>Coordinador - Docente </Link>
                    <div className="white-space-32"></div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(Consumer(Home));
//export default Home;
