import React, { Component } from "react";
import Logo from "../img/logo.jpg";
import { withRouter } from "react-router-dom";
import Request from "../core/httpClient";
import { Consumer } from "../context/";

import Header from "../components/header";


class Reporte extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            message: "",
            periodos: "",
            preguntas: "",
            carrera: "",
            docente: "",
            asignatura: "",
            filtroPeriodo: "",
            filtroTipoEval: "",
            filtroCarrera: "",
            filtroDocente: "",
            filtroAsignatura: "",
            reporte: false

        };
    }

    componentDidMount() {
        this.PeriodoEscolar(this);
    }
    async PeriodoEscolar() {
        this.setState({ loading: true, message: "" });
        const request = new Request();
        const response = await request.post("/reporte/obtenerperiodos");
        if (response.message) { this.setState({ message: response.message }); }
        if (response.periodos) { this.setState({ periodos: response.periodos }); }
        this.setState({ loading: false });
    }
    async Preguntas(event) {
        this.setState({ loading: true, message: "", filtroTipoEval: event.target.value });
        const data = { TipoEnc: parseInt(event.target.value) };
        const request = new Request();
        const response = await request.post("/reporte/obtenerpreguntas", data);
        if (response.message) { this.setState({ message: response.message }); }
        if (response.preguntas) { this.setState({ preguntas: response.preguntas }); }
        this.setState({ loading: false });
    }
    async FiltroPeriodo(event) {
        this.setState({ loading: true, message: "", filtroPeriodo: event.target.value });
        const data = { Periodo: event.target.value };
        const request = new Request();
        const response = await request.post("/reporte/filtroperiodo", data);
        if (response.message) { this.setState({ message: response.message }); }
        if (response.carrera) { this.setState({ carrera: response.carrera }); }
        this.setState({ loading: false });
    }
    async FiltroCarrera(dato) {
        this.setState({ loading: true, message: "", filtroCarrera: dato });
        const data = { Periodo: this.state.filtroPeriodo, Carrera: dato };
        const request = new Request();
        const response = await request.post("/reporte/filtrocarrera", data);
        if (response.message) { this.setState({ message: response.message }); }
        if (response.docente) { this.setState({ docente: response.docente }); }
        this.setState({ loading: false });

    }
    async FiltroDocente(dato) {
        this.setState({ loading: true, message: "", filtroDocente: dato.nombre });
        const data = { Periodo: this.state.filtroPeriodo, Carrera: this.state.filtroCarrera, Docente: dato.docente };
        const request = new Request();
        const response = await request.post("/reporte/filtrodocente", data);
        if (response.message) { this.setState({ message: response.message }); }
        if (response.materia) { this.setState({ asignatura: response.materia }); }
        this.setState({ loading: false });
    }

    async FiltroAsignatura(dato) {
        this.setState({ loading: true, message: "", filtroAsignatura: dato.nombre, reporte: true });
        console.log("Materia", dato)

    }
    pdfToHTML() {

    };

    render() {
        let periodos = "";
        if (this.state.periodos) {
            periodos = this.state.periodos;
        }
        let preguntas = "";
        if (this.state.preguntas) {
            preguntas = this.state.preguntas;
        }
        let Docente = "";
        if (this.state.filtroDocente) {
            Docente = this.state.filtroDocente;
        }
        let Materia = "";
        if (this.state.filtroAsignatura) {
            Materia = this.state.filtroAsignatura;
        }

        return (
            <div className="reports column">
                < Header />
                <div className="content row-responsive">

                    <div className="left column">
                        <div className="title ">
                            <div className="aling-center justify-center optionbtn btn-login  bg-darkBlue color-white font-regular weight-semi" onClick={this.pdfToHTML.bind(this)} >PDF</div>
                        </div>
                        <div className="title">
                            <select className="input-search" type="text" name="search" onChange={this.FiltroPeriodo.bind(this)} >
                                {periodos === "" ?
                                    (<option value="null" >Seleleccione un periodo</option>)
                                    :
                                    (periodos.map((periodo, index) => {
                                        return (
                                            <option key={index} value={periodo.pdocve}>{periodo.pdodes}</option>
                                        );
                                    })
                                    )
                                }
                            </select>
                        </div>


                        <div className="title">
                            <select className="input-search" type="text" name="search" onChange={this.Preguntas.bind(this)} >
                                <option value="null" >Seleccione tipo de Evaluacion</option>
                                <option value={1} >Alumno - Docente</option>
                                <option value={2} >Administrativo - Docente</option>
                                <option value={3} >Docente - Docente</option>
                            </select>
                        </div>
                        <div className="content column">
                            <div className="evaluated-tittle align-center">
                                <div className="name">
                                    <h4 className="font-small">
                                        Carrera
                                </h4>
                                </div>
                            </div>
                            <div className="options column" >
                                {this.state.carrera === "" ?
                                    (<div className="row option" >
                                        <label >Selecciona un periodo escolar</label>
                                    </div>)
                                    :
                                    (this.state.carrera.map((carrera) =>
                                        <div className="row option" key={carrera.carcve} >
                                            <input type="radio" name="answer1" value={carrera.carcve} id={"radioCarrerra-" + carrera.carcve} className="radio" onClick={this.FiltroCarrera.bind(this, carrera.carcve)} />
                                            <label htmlFor={"radioCarrerra-" + carrera.carcve} >{carrera.carnom}</label>
                                        </div>)
                                    )
                                }
                            </div>
                            <div className="evaluated-tittle align-center">
                                <div className="name">
                                    <h4 className="font-small">
                                        Docente
                                </h4>
                                </div>
                            </div>
                            <div className="options column">
                                {this.state.docente === "" ?
                                    (<div className="row option" >
                                        <label >Selecciona una Carrera</label>
                                    </div>)
                                    :
                                    (this.state.docente.map((docente) =>
                                        <div className="row option" key={docente.percve}>
                                            <input type="radio" name="answer2" value={docente.percve} id={"radioDocente-" + docente.percve} className="radio" onClick={this.FiltroDocente.bind(this, { docente: docente.percve, nombre: docente.pernom + " " + docente.perape })} />
                                            <label htmlFor={"radioDocente-" + docente.percve}>{docente.pernom + " " + docente.perape}</label>
                                        </div>)
                                    )
                                }
                            </div>
                            <div className="evaluated-tittle align-center">
                                <div className="name">
                                    <h4 className="font-small">
                                        Asignatura
                                </h4>
                                </div>
                            </div>
                            <div className="options column">
                                {this.state.asignatura === "" ?
                                    (<div className="row option" >
                                        <label >Selecciona un Docente</label>
                                    </div>)
                                    :
                                    (this.state.asignatura.map((asignatura) =>
                                        <div className="row option" key={asignatura.matcve}>
                                            <input type="radio" name="answer3" value={asignatura.matcve} id={"radioAsignatura-" + asignatura.carcve} className="radio" onClick={this.FiltroAsignatura.bind(this, { asignatura: asignatura.matnom, nombre: asignatura.matnom })} />
                                            <label htmlFor={"radioAsignatura-" + asignatura.matcve}>{asignatura.matnom}</label>
                                        </div>)
                                    )
                                }
                            </div>
                        </div>
                        <div className="exit" >
                            <a href="#" className="weight-semi">
                                <i className="fas fa-times"></i>&nbsp; Cerrar sesión
                        </a>
                        </div>
                    </div>
                    <div className="right">
                        <div className="sheet column align-center">
                            <div className="row container">

                                {this.state.reporte === false ?
                                    (
                                        <div className="document column">
                                            <div className="row-responsive">
                                                <div className="column auto">
                                                    <div className="responsive-img item-left justify-center align-center">
                                                        <img src={Logo} alt="responsive img" title="responsive img" className="cover-img logo-doc" />
                                                    </div>
                                                </div>
                                                <div className="column justify-center align-center">
                                                    <h3 className="tittle-doc">
                                                        UNIVERSIDAD POLITECNICA DE QUINTANA ROO
                                                    </h3>
                                                    <div className="white-space-8"></div>
                                                    <p className="font-regular">EVALUACIÓN DOCENTE</p>
                                                    <div className="white-space-8"></div>
                                                </div>
                                            </div>
                                            <div className="white-space-32"></div>
                                            <h1>PREPARANDO REPORTE ....</h1>
                                        </div>
                                    )
                                    :
                                    (
                                        <div className="document column" id="ReporteDocente">
                                            <div className="row-responsive">
                                                <div className="column auto">
                                                    <div className="responsive-img item-left justify-center align-center">
                                                        <img src={Logo} alt="responsive img" title="responsive img" className="cover-img logo-doc" />
                                                    </div>
                                                </div>
                                                <div className="column justify-center align-center">
                                                    <h3 className="tittle-doc">
                                                        UNIVERSIDAD POLITECNICA DE QUINTANA ROO
                                                    </h3>
                                                    <div className="white-space-8"></div>
                                                    <p className="font-regular">EVALUACIÓN DOCENTE</p>
                                                    <div className="white-space-8"></div>
                                                </div>
                                            </div>
                                            <div className="white-space-32"></div>
                                            <div className="row-responsive">
                                                <div className="column column-info">
                                                    <div className="row align-center">
                                                        <div className="column section">
                                                            <p className="font-regular">Profesor : </p>
                                                        </div>
                                                        <div className="column data-content auto" >
                                                            <p className="uppercase">{Docente}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="column column-small align-end">
                                                    <div className="row align-center auto period">
                                                        <div className="column section">
                                                            <p className="font-regular">Periodo :</p>
                                                        </div>
                                                        <div className="column data-content auto" >
                                                            <p>MAY-AGO 2019</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="white-space-16"></div>
                                            <div className="row-responsive">
                                                <div className="column column-info">
                                                    <div className="row align-center">
                                                        <div className="column section">
                                                            <p className="font-regular">Materia : </p>
                                                        </div>
                                                        <div className="column data-content auto" >
                                                            <p>{Materia}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="column column-small align-end">
                                                    <div className="row align-center">
                                                        <div className="column alumns">
                                                            <p className="font-regular">Numero alumnos :</p>
                                                        </div>
                                                        <div className="column data-content auto" >
                                                            <p>25</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="white-space-32"></div>
                                            <div className="white-space-8"></div>
                                            <table className="table-report">
                                                <thead>
                                                    <tr>
                                                        <th>PREGUNTA</th>
                                                        <th>VALORACION</th>
                                                        <th>CALIFICACION</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="capitalize">
                                                    {preguntas === "" ?
                                                        (<tr>
                                                            <td> </td>
                                                            <td> </td>
                                                            <td> </td>

                                                        </tr>)
                                                        :
                                                        (preguntas.map((pregunta, index) => {
                                                            return (
                                                                <tr key={pregunta.idpregunta}>
                                                                    <td> {index}.- {pregunta.pregunta} </td>
                                                                    <td> BUENO </td>
                                                                    <td> 9.00 </td>

                                                                </tr>)
                                                        }
                                                        ))
                                                    }
                                                    <tr>
                                                        <td></td>
                                                        <td className="final">PROMEDIO TOTAL</td>
                                                        <td className="final">9.00</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div className="white-space-24"></div>
                                            <div className="row">
                                                <div className="column score">
                                                    <div className="row">
                                                        <p className="font-tiny">Deficiente 1 - 1.9</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="white-space-8"></div>
                                            <div className="row">
                                                <div className="column score">
                                                    <div className="row">
                                                        <p className="font-tiny">Competente 2 - 2.9</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="white-space-8"></div>
                                            <div className="row">
                                                <div className="column score">
                                                    <div className="row">
                                                        <p className="font-tiny">Bueno 3 - 3.5</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="white-space-8"></div>
                                            <div className="row">
                                                <div className="column score">
                                                    <div className="row">
                                                        <p className="font-tiny">Excelente 3.6 - 4</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }


                            </div>
                            <div className="white-space-32"></div>

                        </div>

                    </div>
                </div>
            </div>
        )
    }
} export default withRouter(Consumer(Reporte));


