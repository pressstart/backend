import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Consumer } from "../context";
import Fondo from "../img/leftblue.jpg"
import Logo from "../img/upqroo.png"

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
  //  console.log(JSON.parse(window.localStorage.getItem("tipo_eval")).typeUser)
  }
  Alumno() {
    return (
      <div className="column login">
        <div className="row background-login" >
          <div className="column justify-center align-center">
            <div className="row">
              <div className="column left justify-center align-center" style={{ backgroundImage: "url(" + Fondo + ")" }}>
                <div className="container responsive-img item-left justify-center align-center">
                  <img src={Logo} alt="Universidad Politecnica" title="Universidad Politecnica" className="cover-img logo" />
                </div>
              </div>

              <div className="column right align-center justify-center">
                <div className="row container justify-center">
                  <form className="column form-data" action="/surveys.html">
                    <h1 className="color-darkBlue font-double text-center title">EVALUACIÓN DOCENTE</h1>
                    <div className="white-space-24"></div>
                    <p className="color-darkgray font-regular text-center">Introduzca sus datos de acceso</p>
                    <div className="white-space-32"></div>

                    <input type="text" name="matricula" placeholder="Matricula" className="input input-form" required />
                    <div className="white-space-24"></div>
                    <input type="password" name="matricula" placeholder="Contraseña" className="input input-form" required />
                    <div className="white-space-32"></div>
                    <button type="submit" className="btn btn-login  bg-darkBlue color-white font-regular weight-semi">INGRESAR</button>
                    <div className="white-space-24"></div>
                    <div className="row justify-center">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>)
  }
  Docente() {
    return (
      <div className="column login">
        <div className="row background-login" >
          <div className="column justify-center align-center">
            <div className="row">
              <div className="column left justify-center align-center" style={{ backgroundImage: "url(" + Fondo + ")" }}>
                <div className="container responsive-img item-left justify-center align-center">
                  <img src={Logo} alt="Universidad Politecnica" title="Universidad Politecnica" className="cover-img logo" />
                </div>
              </div>

              <div className="column right align-center justify-center">
                <div className="row container justify-center">
                  <form className="column form-data" action="/surveys.html">
                    <h1 className="color-darkBlue font-double text-center title">EVALUACIÓN DOCENTE</h1>
                    <div className="white-space-24"></div>
                    <p className="color-darkgray font-regular text-center">Introduzca sus datos de acceso</p>
                    <div className="white-space-32"></div>

                    <input type="text" name="matricula" placeholder="Matricula" className="input input-form" required />
                    <div className="white-space-24"></div>
                    <input type="password" name="matricula" placeholder="Contraseña" className="input input-form" required />
                    <div className="white-space-32"></div>
                    <button type="submit" className="btn btn-login  bg-darkBlue color-white font-regular weight-semi">INGRESAR</button>
                    <div className="white-space-24"></div>
                    <div className="row justify-center">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>)
  }

  render() {


    return (
      <div >
        {/*JSON.parse(window.localStorage.getItem("tipo_eval")).typeUser === "alumno" ? this.Alumno() : this.Docente()*/}
        {this.Alumno()}
      </div>

    );
  }
}
//export default Login;
export default withRouter(Consumer(Login));