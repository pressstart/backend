import React, { Component } from "react";
import Fondo from "../img/header.png"

class header extends Component {
  render() {
    return (
      <div className="row-responsive justify-center header-tittle align-center" style={{backgroundImage: "url("+Fondo+")"}}>
                <div className=" align-center header-content ">
                    <div className="row auto">
                        <div className="column auto justify-center">
                            <h1 className="color-white weight-bold">Sistema de evaluación docente</h1>
                            <h3 className="color-white font-medium weight-regular">Seleccione el profesor a evaluar</h3>
                        </div>  
                    </div>
                </div>
       </div>
    );
  }
}
export default header;
