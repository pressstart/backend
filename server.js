const http        		= require("http");
const koa         		= require("koa");
const app         		= new koa();
const cors        		= require("koa2-cors");
const socket			= require('socket.io');

/**
 *  	Variables de entorno / 	HOST: String, PORT: Int
 */

const HOST = process.env.HOST || "http://localhost";
const PORT = process.env.PORT || 8081;

// Declaracion del servidor

const server = http.createServer(app.callback());

const io = socket(server);
// Gestión de las rutas válidas de la aplicación

const { modules } = require("./src/modules/index")(io);
modules.map(route => {
	app.use(route);
});

/**
 * 	Ejecución del servidor.
 * 	Activa el evento "Listener" este método es llamado cuando un evento ocurre.
 */

server.listen(PORT, () => {
	console.log("Aplicación en ejecución:", `${HOST}:${PORT}`);
});