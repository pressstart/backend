/**
 *  index.js
 *  @version: 1.0.0
 *  @author: Fernando Aban
 *  @description: Funciones asyncronas para las peticiones a bases de datos /user
 */

const config = require('../../config');
const mysql = require("mysql2/promise");

async function obtenerperiodos(data) {
  try {
    const storedProcedure = `CALL prd_RecuperaPeriodo()`;
    const connection = await new mysql.createPool(config);
    const user = await connection
      .query(storedProcedure)
      .then(async ([rows]) => {
        const recordset = rows[0];
        console.log(recordset)
        if (recordset) {
          if (recordset.length > 0) {
            return { periodos: recordset, message: 'DATOS DEVUELTOS' };
          } else {
            return { error: true, message: 'ERROR AL RECUPERAR LOS DATOS' };
          }
        } else {
          return { error: true, message: 'ERROR INTERNO' };
        }
      });
    connection.end();
    return user;
  } catch (error) {
    return { error: true, message: error.message };
  }
}
async function obtenerpreguntas(data) {
  try { 
    console.log(data)
    const TipoEnc = (data.TipoEnc);
    const storedProcedure = `CALL prd_Preguntas(?)`;
    const connection = await new mysql.createPool(config);
    const user = await connection
      .query(storedProcedure,[TipoEnc])
      .then(async ([rows]) => {
        const recordset = rows[0];
        console.log(recordset)
        if (recordset) {
          if (recordset.length > 0) {
            return { preguntas: recordset, message: 'DATOS DEVUELTOS' };
          } else {
            return { error: true, message: 'ERROR AL RECUPERAR LOS DATOS' };
          }
        } else {
          return { error: true, message: 'ERROR INTERNO' };
        }
      });
    connection.end();
    return user;
  } catch (error) {
    return { error: true, message: error.message };
  }
}

async function filtroperiodo(data) {
  try {
    const Periodo = (data.Periodo);
    const storedProcedure = `CALL prd_BusquedaPeriodoEscolar(?)`;
    const connection = await new mysql.createPool(config);
    const user = await connection
      .query(storedProcedure, [Periodo])
      .then(async ([rows]) => {
        const recordset = rows[0];
        console.log(recordset)
        if (recordset) {
          if (recordset.length > 0) {
            return { carrera: recordset, message: 'DATOS DEVUELTOS' };
          } else {
            return { error: true, message: 'ERROR AL RECUPERAR LOS DATOS' };
          }
        } else {
          return { error: true, message: 'ERROR INTERNO' };
        }
      });
    connection.end();
    return user;
  } catch (error) {
    return { error: true, message: error.message };
  }
}
async function filtrocarrera(data) {
  try {
    const Periodo = (data.Periodo);
    const Carrera = (data.Carrera);
    const storedProcedure = `CALL prd_BusquedaCarrera(?,?)`;
    const connection = await new mysql.createPool(config);
    const user = await connection
      .query(storedProcedure, [Periodo, Carrera])
      .then(async ([rows]) => {
        const recordset = rows[0];
        console.log(recordset)
        if (recordset) {
          if (recordset.length > 0) {
            return { docente: recordset, message: 'DATOS DEVUELTOS' };
          } else {
            return { error: true, message: 'ERROR AL OBTENER LOS DOCENTES' };
          }
        } else {
          return { error: true, message: 'ERROR INTERNO' };
        }
      });
    connection.end();
    return user;
  } catch (error) {
    return { error: true, message: error.message };
  }
}
async function filtrodocente(data) {
  try {
    const Periodo = (data.Periodo);
    const Carrera = (data.Carrera);
    const Docente = (data.Docente)
    const storedProcedure = `CALL prd_BusquedaDocente(?,?,?)`;
    const connection = await new mysql.createPool(config);
    const user = await connection
      .query(storedProcedure, [Periodo, Carrera, Docente])
      .then(async ([rows]) => {
        const recordset = rows[0];
        console.log(recordset)
        if (recordset) {
          if (recordset.length > 0) {
            return { materia: recordset, message: 'DATOS DEVUELTOS' };
          } else {
            return { error: true, message: 'ERROR AL RECUPERAR LOS DATOS' };
          }
        } else {
          return { error: true, message: 'ERROR INTERNO' };
        }
      });
    connection.end();
    return user;
  } catch (error) {
    return { error: true, message: error.message };
  }
}
module.exports = {
  obtenerperiodos,
  obtenerpreguntas,
  filtroperiodo,
  filtrocarrera,
  filtrodocente
};