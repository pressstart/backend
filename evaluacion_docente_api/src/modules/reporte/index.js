/**
 *  index.js
 *  @version: 1.0.0
 *  @author:  Fernando
 *  @description: Rutas y métodos para /users
 */
const koaRouter = require("koa-router");
const koaBody = require("koa-body");
const db = require('../../controllers/reporte');

const router = new koaRouter({
	prefix: '/reporte'
});
router.post('/obtenerperiodos', koaBody(), async function (context) {
	try {
		const data = context.request.body;
		console.log(data);
		context.body = await db.obtenerperiodos();
	} catch (error) {
		context.body = { error: true, message: error.message };
	}
});

router.post('/obtenerpreguntas', koaBody(), async function (context) {
	try {
		const data = context.request.body;
		console.log(data);
		context.body = await db.obtenerpreguntas(data);
	} catch (error) {
		context.body = { error: true, message: error.message };
	}
});
router.post('/filtroperiodo', koaBody(), async function (context) {
	try {
		const data = context.request.body;
		console.log(data);
		context.body = await db.filtroperiodo(data);
	} catch (error) {
		context.body = { error: true, message: error.message };
	}
});
router.post('/filtrocarrera', koaBody(), async function (context) {
	try {
		const data = context.request.body;
		console.log(data);
		context.body = await db.filtrocarrera(data);
	} catch (error) {
		context.body = { error: true, message: error.message };
	}
});
router.post('/filtrodocente', koaBody(), async function (context) {
	try {
		const data = context.request.body;
		console.log(data);
		context.body = await db.filtrodocente(data);
	} catch (error) {
		context.body = { error: true, message: error.message };
	}
});
module.exports = router;