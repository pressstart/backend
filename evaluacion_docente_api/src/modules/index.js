/**
 *  index.js
 *  @version: 1.0.0
 *  @author: Fernando Aban
 *  @description: Incorporación de todas las rutas de la aplicación
 */

function modules(io) {
	const index 	= require("./default");
	const reporte 	= require("./reporte/index");
	
	return {
		modules: [		
			index.routes(),	
			reporte.routes(),		
		]
	};
}

module.exports = modules;