/**
 *  index.js
 *  @version: 1.0.0
 *  @author:  Fernando
 *  @description: Rutas y métodos /
 */

const koaRouter = require("koa-router");
const router = new koaRouter();

router.get("/", async function(context) {
	context.body = "prueba - 2019";
});

module.exports = router;