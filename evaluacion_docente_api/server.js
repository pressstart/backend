const http        		= require("http");
const koa         		= require("koa");
const app         		= new koa();
const cors        		= require("koa2-cors");
const socket			= require('socket.io');

// Cross - Origin Resource Sharing(CORS)

app.use(
	cors({
		origin: function() {
	  		if (process.env.NODE_ENV !== "production") {
				return "*";
	  		}
	  		return "*";
		}
  	})
);

app.use(async (context, next) => {
	const { getDecode } = require('./src/auth/token');
	function revisar() {
	  try {
		let auth = getDecode(context.headers.auth);
		if(auth) {
		  auth = JSON.parse(auth);
		  if(auth.id === process.env.IDAPP) {
			return true;
		  } else {
			return false;
		  }
		}
	  } catch(error) {
		console.log(error.message);
		return false;
	  }
	}
   
	  await next();
	const rt = context.response.get("X-Response-Time");
	console.log(`${context.method} ${context.url} - ${rt}`);
	console.log(`Status: ${context.status}`);
  });

app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set("X-Response-Time", `${ms}ms`);
});

const HOST = process.env.HOST || "http://localhost";
const PORT = process.env.PORT || 8081;

// Declaracion del servidor

const server = http.createServer(app.callback());

const io = socket(server);

// Gestión de las rutas válidas de la aplicación

const { modules } = require("./src/modules/index")(io);
modules.map(route => {
	app.use(route);
});

/**
 * 	Ejecución del servidor.
 * 	Activa el evento "Listener" este método es llamado cuando un evento ocurre.
 */

server.listen(PORT, () => {
	console.log("Aplicación en ejecución:", `${HOST}:${PORT}`);
});