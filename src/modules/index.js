
function modules(io) {
const alumnos = require("../database/validacion.js");
const grupos = require("../database/bulkcopygrupos.js");
const listas = require("../database/bulkcopylistas.js");
const materias = require("../database/bulkcopymateria.js");
const personal = require("../database/bulkcopypersonal.js");
const periodo = require("../database/bulkcopyperiodo.js");
const carreras = require("../database/bulkcopycarreras.js");
const index = require("../database/default.js");


	
	return {
		modules: [

			alumnos.routes(),
			grupos.routes(),
			listas.routes(),
			materias.routes(),
			personal.routes(),
			periodo.routes(),
			carreras.routes(),
			index.routes()
		
		]
	};
}

module.exports = modules;