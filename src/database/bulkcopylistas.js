const koaRouter = require("koa-router");
const router = new koaRouter();
var Parser = require('dbf-parser');
var parser = new Parser('../backend/src/database/DBFSIEVAL/DLISTA.DBF');

var con = require('../config/config.js');
var PDOCVE = [];
var MATCVE = [];
var GPOCVE = [];
var ALUCTR = [];
var i=0;
var mensaje="";


function bulkcopylistas(a,b,c){
   
  
      var sql = "INSERT INTO lista (pdocve, aluctr, matcve, idgpo) VALUES  ('"+a+"','"+b+"','"+c+"',NULL)";
      con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("record inserted No. "+i++);
      });
    }
  


parser.on('start', function(p) {
    console.log('dBase file parsing has started');
});

parser.on('header', function(h) {
    console.log('dBase file header has been parsed');
});

parser.on('record', function(record) {
          
        console.log(record.PDOCVE + " " + record.ALUCTR+ " " +  record.MATCVE + " " +  record.GPOCVE); 

      bulkcopylistas(record.PDOCVE,record.ALUCTR,record.MATCVE);
   
    
});

parser.on('end', function(p) {
    console.log('Finished parsing the dBase file');

    con.end();
    module.exports = { numero: GPOCVE, periodo: PDOCVE, alumno: ALUCTR, materia:MATCVE};
  
});

router.get("/bulkcopylistas", async function(context) {
    try{
        parser.parse();
        context.body = "copiado exitosamente";
               
     }
        catch (error){
            context.body = "error al copiar los datos";
        }


});

module.exports = router;